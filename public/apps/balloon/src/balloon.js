import { LoaderScene } from "./scenes/loader_scene.js";
import { MenuScene } from "./scenes/menu_scene.js";

class Balloon {
  constructor() {
    // dimensions
    this.width = 1194.0;
    this.height = 758.0;
    this.screenWidth = 0;
    this.screenHeight = 0;

    // setup
    this.setupApp();
    this.setupRootView();
    this.watchWindowSize();
    //this.registerServiceWorker();

    // start
    this.switchScene(
      new LoaderScene(this, () => {
        this.switchScene(new MenuScene(this, 0));
      })
    );
  }

  // create app
  setupApp() {
    this.app = new PIXI.Application({
      width: window.innerWidth,
      height: window.innerHeight,
      autoDensity: true,
      resolution: window.devicePixelRatio,
    });
    document.body.appendChild(this.app.view);
  }

  // create root container
  setupRootView() {
    // create container
    this.root = new PIXI.Container();
    this.app.stage.addChild(this.root);
    this.resizeRootView();
  }

  resizeRootView() {
    // did window size even change?
    if (
      this.screenWidth == window.innerWidth &&
      this.screenHeight == window.innerHeight
    ) {
      return;
    }

    let x = 0;
    let y = 0;

    // update screen
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;

    // resize renderer
    this.app.renderer.resize(this.screenWidth, this.screenHeight);

    // calc scale factor based on available width
    this.scale = this.screenWidth / this.width;

    // center in y axis
    let actualHeight = this.height * this.scale;
    y = (this.screenHeight - actualHeight) / 2;

    // check: is higher than available screen?
    if (actualHeight > this.screenHeight) {
      // calc scale factor based on available height
      this.scale = this.screenHeight / this.height;

      // center in x axis
      let actualWidth = this.width * this.scale;
      x = (this.screenWidth - actualWidth) / 2;
      y = 0;
    }

    // update container
    this.root.scale.x = this.scale;
    this.root.scale.y = this.scale;
    this.root.y = y;
    this.root.x = x;
  }

  watchWindowSize() {
    // mobile browsers are very inconsistent with
    // updating window.innerWidth after orientation change.
    // hence we send multiple delayed resize events
    const delays = [0, 50, 1000];

    window.addEventListener("resize", () => {
      for (const delay of delays) {
        setTimeout(() => {
          this.resizeRootView();
        }, delay);
      }
    });
  }

  // switch to new scene
  switchScene(newScene) {
    // remove current scene
    if (this.scene) {
      this.root.removeChild(this.scene.stage);
    }

    // add new
    this.scene = newScene;
    this.root.addChild(this.scene.stage);
  }

  // retrieve highscore data
  getHighscoreFor(level) {
    const data = localStorage.getItem("level" + level);
    if (!data) {
      return false;
    }
    return JSON.parse(data);
  }

  // store highscore data
  setHighscoreFor(level, data) {
    localStorage.setItem("level" + level, JSON.stringify(data));
  }

  registerServiceWorker() {
    try {
      navigator.serviceWorker.register("/apps/balloon/service_worker.js", {
        scope: "/apps/balloon/",
      });
    } catch (error) {
      console.error("service worker registration failed", error);
    }
  }
}

window.addEventListener("DOMContentLoaded", (e) => {
  window.balloon = new Balloon();
});
