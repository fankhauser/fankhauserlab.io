import { MenuScene } from "./menu_scene.js";
import { NewHighscoreScene } from "./new_highscore_scene.js";
import { balloonTypeToImageMap } from "../shared.js";

export class LevelScene {
  constructor(balloon, level) {
    this.balloon = balloon;

    // config
    this.viewWidth = this.balloon.width;
    this.viewHeight = this.balloon.height - 60;
    this.viewBorder = 20;
    this.viewY = 60;
    this.balloonCountX = 12;
    this.balloonCountY = 8;
    this.balloonWidth = 64;
    this.balloonHeight = 69;
    this.balloonSpaceX =
      (this.viewWidth -
        2 * this.viewBorder -
        this.balloonCountX * this.balloonWidth) /
      (this.balloonCountX - 1);
    this.balloonSpaceY =
      (this.viewHeight -
        2 * this.viewBorder -
        this.balloonCountY * this.balloonHeight) /
      (this.balloonCountY - 1);

    // stage
    this.stage = new PIXI.Container();

    // setup
    this.setupLevel(level);
    this.setupBackground();
    this.setupCounts();
    this.setupText();
    this.setupView();

    this.updateCounts();
    this.startTime();
  }

  setupLevel(level) {
    this.level = level;
    this.levelData = PIXI.Loader.shared.resources.levels.data[this.level];
  }

  setupBackground() {
    this.bg = new PIXI.Sprite(PIXI.Loader.shared.resources.level.texture);
    this.bg.width = this.balloon.width;
    this.bg.height = this.balloon.height;
    this.stage.addChild(this.bg);
  }

  setupCounts() {
    this.totalBalloons = 0;
    this.foundBalloons = 0;

    for (let x = 0; x < this.balloonCountX; x++) {
      for (let y = 0; y < this.balloonCountY; y++) {
        let type = this.levelData.matrix[y][x];
        // yellow balloons count
        if (type === 1) {
          this.totalBalloons += 1;
        }
      }
    }
  }

  setupText() {
    let style = new PIXI.TextStyle({
      fontFamily: "Arial",
      fontSize: 28,
      fill: 0xffffff,
      align: "left",
      dropShadow: true,
      dropShadowBlur: 5,
      dropShadowDistance: 3,
      dropShadowAlpha: 0.5,
      fontWeight: "500",
    });

    // level
    this.levelText = new PIXI.Text(this.levelData.name, style);
    this.levelText.style.align = "left";
    this.levelText.anchor.set(0, 0);
    this.levelText.x = this.viewBorder;
    this.levelText.y = this.viewBorder;
    this.stage.addChild(this.levelText);

    // time
    this.timeText = new PIXI.Text("0.0s", style);
    this.timeText.style.align = "center";
    this.timeText.anchor.set(0.5, 0);
    this.timeText.x = this.balloon.width / 2;
    this.timeText.y = this.viewBorder;
    this.stage.addChild(this.timeText);

    // count
    this.countText = new PIXI.Text("0/0", style);
    this.countText.style.align = "right";
    this.countText.anchor.set(1, 0);
    this.countText.x = this.balloon.width - this.viewBorder;
    this.countText.y = this.viewBorder;
    this.stage.addChild(this.countText);
  }

  updateCounts() {
    this.countText.text = `${this.foundBalloons}/${this.totalBalloons}`;
  }

  setupView() {
    this.view = new PIXI.Container();
    this.view.pivot.set(this.viewWidth / 2, 0);
    this.view.x = this.balloon.width / 2;
    this.view.y = this.viewY;
    this.stage.addChild(this.view);

    // paint all balloons
    for (let x = 0; x < this.balloonCountX; x++) {
      for (let y = 0; y < this.balloonCountY; y++) {
        // balloon type
        let type = this.levelData.matrix[y][x];

        // no balloon
        if (type === 0) {
          continue;
        }

        // load image
        let image = balloonTypeToImageMap[type];
        let balloon = new PIXI.Sprite(
          PIXI.Loader.shared.resources[image].texture
        );
        balloon.width = this.balloonWidth;
        balloon.height = this.balloonHeight;
        balloon.x =
          this.viewBorder +
          x * (this.balloonWidth + this.balloonSpaceX) +
          this.balloonWidth / 2;
        balloon.y =
          this.viewBorder +
          y * (this.balloonHeight + this.balloonSpaceY) +
          this.balloonHeight / 2;

        balloon.anchor.set(0.5, 0.5);

        // balloon action
        balloon.interactive = true;
        balloon.buttonMode = true;
        balloon.on("pointerdown", () => {
          // disable
          balloon.interactive = false;
          balloon.buttonMode = false;

          switch (type) {
            // yellow balloon
            case 1:
              this.explodeYellowBalloon(balloon);
              break;

            // bomb
            case 2:
              this.explodeBombBalloon(balloon);
              break;

            default:
              break;
          }
        });

        this.view.addChild(balloon);
      }
    }
  }

  explodeYellowBalloon(balloon) {
    // plop sound
    PIXI.Loader.shared.resources.plop.sound.play();

    // setup animation
    let steps = 0;
    let duration = 5;
    let progress = 0;
    let startScale = balloon.scale.x;
    let offset = 400;
    let dx = Math.random() * offset - offset / 2;
    let dy = Math.random() * offset - offset / 2;
    let dxStep = dx / duration;
    let dyStep = dy / duration;
    let scale = Math.random() * 0.7;
    const animation = (delta) => {
      steps += 1;
      progress = steps / duration;

      // fadeout
      balloon.alpha = 1 - progress;

      // scale up
      balloon.scale.x = startScale + progress * scale;
      balloon.scale.y = startScale + progress * scale;

      // move
      balloon.x += dxStep;
      balloon.y += dyStep;

      // end
      if (steps > duration) {
        this.balloon.app.ticker.remove(animation);
        this.view.removeChild(balloon);
      }
    };
    this.balloon.app.ticker.add(animation);

    // update counters
    this.foundBalloons++;
    this.updateCounts();

    // check all found
    if (this.foundBalloons >= this.totalBalloons) {
      this.stopTime();
      this.endWithSuccess();
    }
  }

  explodeBombBalloon(balloon) {
    this.view.removeChildren();
    this.view.addChild(balloon);

    // setup animation
    let steps = 0;
    let duration = 8;
    let progress = 0;
    let startScale = balloon.scale.x;
    let scale = 0.8;
    const animation = (delta) => {
      steps += 1;
      progress = steps / duration;

      // fadeout
      balloon.alpha = 1 - progress;

      // scale up
      balloon.scale.x = startScale + progress * scale;
      balloon.scale.y = startScale + progress * scale;

      // end
      if (steps > duration) {
        this.balloon.app.ticker.remove(animation);
        this.view.removeChild(balloon);
      }
    };
    this.balloon.app.ticker.add(animation);
    this.stopTime();
    this.endWithBomb();
  }

  setupButtons() {
    // next level
    let next = new PIXI.Graphics();
    next.hitArea = new PIXI.Rectangle(901, 421, 108, 150);
    next.interactive = true;
    next.buttonMode = true;
    next.on("pointerup", () => {
      this.updateLevel(1);
    });
    this.stage.addChild(next);

    // previous level
    let previous = new PIXI.Graphics();
    previous.hitArea = new PIXI.Rectangle(187, 421, 108, 150);
    previous.interactive = true;
    previous.buttonMode = true;
    previous.on("pointerup", () => {
      this.updateLevel(-1);
    });
    this.stage.addChild(previous);
  }

  startTime() {
    this.elapsed = 0.0;
    this.balloon.app.ticker.add((delta) => {
      this.elapsed += this.balloon.app.ticker.deltaMS;
    });
    this.timeRunning = true;
    this.updateTime();
  }

  updateTime() {
    this.timeText.text = (this.elapsed / 1000.0).toFixed(1) + "s";
    if (!this.timeRunning) {
      return;
    }

    setTimeout(() => {
      this.updateTime();
    }, 100);
  }

  stopTime() {
    this.timeRunning = false;
  }

  endWithSuccess() {
    // fetch current highscore
    const highscore = this.balloon.getHighscoreFor(this.level);
    const playerWasFaster = this.elapsed < highscore.time;

    // new highscore
    if (!highscore || playerWasFaster) {
      this.balloon.switchScene(
        new NewHighscoreScene(this.balloon, this.level, this.elapsed)
      );
      return;
    }

    // too slow
    // play sound
    PIXI.Loader.shared.resources.elevator.sound.play();

    // show info
    let tooSlow = new PIXI.Sprite(
      PIXI.Loader.shared.resources.too_slow.texture
    );
    tooSlow.width = 800;
    tooSlow.height = 225;
    tooSlow.pivot.set(800, 225);
    tooSlow.x = this.balloon.width / 2;
    tooSlow.y = this.balloon.height / 2;
    this.stage.addChild(tooSlow);

    // go back to menu
    setTimeout(() => {
      this.balloon.switchScene(new MenuScene(this.balloon, this.level));
    }, 5000);
  }

  endWithBomb() {
    // play sound
    PIXI.Loader.shared.resources.explode.sound.play();

    // show info
    let gameOver = new PIXI.Sprite(
      PIXI.Loader.shared.resources.game_over.texture
    );
    gameOver.width = 800;
    gameOver.height = 225;
    gameOver.pivot.set(800, 225);
    gameOver.x = this.balloon.width / 2;
    gameOver.y = this.balloon.height / 2;
    this.stage.addChild(gameOver);

    // go back to menu
    setTimeout(() => {
      this.balloon.switchScene(new MenuScene(this.balloon, this.level));
    }, 3000);
  }
}
