// assets to load
const items = {
  menu: "menu.png",
  level: "level.png",
  new_highscore: "new_highscore.png",
  balloon_yellow: "balloon_yellow.png",
  balloon_black: "balloon_black.png",
  game_over: "game_over.png",
  too_slow: "too_slow.png",
  levels: "levels.json",
  plop: "plop.mp3",
  explode: "explode.mp3",
  applause: "applause.mp3",
  elevator: "elevator.mp3",
};

function gradient(from, to) {
  const c = document.createElement("canvas");
  const ctx = c.getContext("2d");
  const grd = ctx.createLinearGradient(0, 0, 100, 100);
  grd.addColorStop(0, from);
  grd.addColorStop(1, to);
  ctx.fillStyle = grd;
  ctx.fillRect(0, 0, 100, 100);
  return new PIXI.Texture.from(c);
}

export class LoaderScene {
  constructor(balloon, done) {
    this.balloon = balloon;

    // config
    this.barWidth = this.balloon.width / 4;
    this.barHeight = 20;
    this.barY = this.balloon.height / 2 + 10;
    this.barX = this.balloon.width / 2 - this.barWidth / 2;

    // stage
    this.stage = new PIXI.Container();

    this.setupBackground();
    this.setupText();
    this.setupProgressBar();
    this.startLoading(done);
  }

  setupBackground() {
    this.bg = new PIXI.Graphics();
    this.bg.beginFill(0x3d8cc8);
    this.bg.drawRect(0, 0, this.balloon.width, this.balloon.height);
    this.stage.addChild(this.bg);
  }

  setupProgressBar() {
    // bg
    this.barBg = new PIXI.Graphics();
    this.barBg.beginFill(0xa0ccea);
    this.barBg.drawRect(this.barX, this.barY, this.barWidth, this.barHeight);
    this.stage.addChild(this.barBg);

    // fg
    this.updateProgressBar(0);
  }

  updateProgressBar(percent) {
    // clear old
    if (this.barFg) {
      this.stage.removeChild(this.barFg);
    }

    // add
    let width = this.barWidth * percent;
    this.barFg = new PIXI.Graphics();
    this.barFg.beginFill(0xffffff);
    this.barFg.drawRect(this.barX, this.barY, width, this.barHeight);
    this.stage.addChild(this.barFg);
  }

  setupText() {
    let style = new PIXI.TextStyle({
      fontFamily: "Arial",
      fontSize: 24,
      fill: 0xffffff,
      align: "center",
      dropShadow: true,
      dropShadowBlur: 5,
      dropShadowDistance: 3,
      dropShadowAlpha: 0.5,
      fontWeight: "500",
    });

    // loading text
    this.loadingText = new PIXI.Text("Loading 0%", style);
    this.loadingText.style.align = "center";
    this.loadingText.anchor.set(0.5);
    this.loadingText.x = this.balloon.width / 2;
    this.loadingText.y = this.balloon.height / 2 - 10;
    this.stage.addChild(this.loadingText);
  }

  startLoading(done) {
    // assets path
    const assets = "/apps/balloon/assets/";

    // add items
    let total = 0;
    const loader = PIXI.Loader.shared;
    for (const key in items) {
      loader.add(key, assets + items[key]);
      total += 1;
    }

    let loaded = 0;
    let percent = 0;
    loader.onProgress.add(() => {
      loaded += 1;
      percent = parseFloat(loaded) / parseFloat(total);
      this.loadingText.text = "Loading " + (percent * 100.0).toFixed(0) + "%";
      this.updateProgressBar(percent);
    });

    // called once done
    loader.load((loader, resources) => {
      done();
    });
  }
}
