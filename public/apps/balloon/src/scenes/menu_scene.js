import { balloonTypeToImageMap } from "../shared.js";
import { LevelScene } from "./level_scene.js";

export class MenuScene {
  constructor(balloon, level) {
    this.balloon = balloon;

    // config
    this.previewWidth = 500;
    this.previewHeight = 317;
    this.previewBorder = 10;
    this.previewY = 328;
    this.balloonCountX = 12;
    this.balloonCountY = 8;
    this.balloonWidth = 32;
    this.balloonHeight = 34;
    this.balloonSpaceX =
      (this.previewWidth -
        2 * this.previewBorder -
        this.balloonCountX * this.balloonWidth) /
      (this.balloonCountX - 1);
    this.balloonSpaceY =
      (this.previewHeight -
        2 * this.previewBorder -
        this.balloonCountY * this.balloonHeight) /
      (this.balloonCountY - 1);

    // stage
    this.stage = new PIXI.Container();

    // setup
    this.setupLevels(level);
    this.setupBackground();
    this.setupText();
    this.setupPreview();
    this.setupButtons();

    this.updateLevel(0);
  }

  setupLevels(level) {
    this.levels = PIXI.Loader.shared.resources.levels.data;
    this.level = level || 0;
    this.maxLevel = this.levels.length - 1;
  }

  setupBackground() {
    this.bg = new PIXI.Sprite(PIXI.Loader.shared.resources.menu.texture);
    this.bg.width = this.balloon.width;
    this.bg.height = this.balloon.height;
    this.stage.addChild(this.bg);
  }

  setupText() {
    let style = new PIXI.TextStyle({
      fontFamily: "Arial",
      fontSize: 24,
      fill: 0xffffff,
      align: "center",
      dropShadow: true,
      dropShadowBlur: 5,
      dropShadowDistance: 3,
      dropShadowAlpha: 0.5,
      fontWeight: "500",
    });

    // level text
    this.levelText = new PIXI.Text(this.levels[this.level].name, style);
    this.levelText.style.align = "center";
    this.levelText.anchor.set(0.5);
    this.levelText.x = this.balloon.width / 2;
    this.levelText.y = 290;
    this.stage.addChild(this.levelText);

    // highscore text
    this.highscoreText = new PIXI.Text("Noch kein Highscore", style);
    this.highscoreText.anchor.set(0.5);
    this.highscoreText.x = this.balloon.width / 2;
    this.highscoreText.y = 685;
    this.stage.addChild(this.highscoreText);
  }

  setupPreview() {
    this.preview = new PIXI.Container();
    this.preview.pivot.set(this.previewWidth / 2, 0);
    this.preview.x = this.balloon.width / 2;
    this.preview.y = this.previewY;
    this.preview.interactive = true;
    this.preview.buttonMode = true;
    this.preview.on("pointerup", () => {
      this.balloon.switchScene(new LevelScene(this.balloon, this.level));
    });
    this.stage.addChild(this.preview);
  }

  updatePreview() {
    // clear
    this.preview.removeChildren();

    // transparent bg
    let bg = new PIXI.Graphics();
    bg.hitArea = new PIXI.Rectangle(
      0,
      0,
      this.previewWidth,
      this.previewHeight
    );
    this.preview.addChild(bg);

    // paint all balloons
    for (let x = 0; x < this.balloonCountX; x++) {
      for (let y = 0; y < this.balloonCountY; y++) {
        // balloon type
        let type = this.levels[this.level].matrix[y][x];

        // no balloon
        if (type === 0) {
          continue;
        }

        // load image
        let image = balloonTypeToImageMap[type];
        let balloon = new PIXI.Sprite(
          PIXI.Loader.shared.resources[image].texture
        );
        balloon.width = this.balloonWidth;
        balloon.height = this.balloonHeight;
        balloon.x =
          this.previewBorder + x * (this.balloonWidth + this.balloonSpaceX);
        balloon.y =
          this.previewBorder + y * (this.balloonHeight + this.balloonSpaceY);

        this.preview.addChild(balloon);
      }
    }

    // update level text
    this.levelText.text = this.levels[this.level].name;

    // update highscore
    const highscore = this.balloon.getHighscoreFor(this.level);
    if (!highscore) {
      this.highscoreText.text = "Noch kein Highscore";
    } else {
      this.highscoreText.text = `${(highscore.time / 1000).toFixed(2)}s von ${
        highscore.name
      }`;
    }
  }

  setupButtons() {
    // next level
    let next = new PIXI.Graphics();
    next.hitArea = new PIXI.Rectangle(901, 421, 108, 150);
    next.interactive = true;
    next.buttonMode = true;
    next.on("pointerup", () => {
      this.updateLevel(1);
    });
    this.stage.addChild(next);

    // previous level
    let previous = new PIXI.Graphics();
    previous.hitArea = new PIXI.Rectangle(187, 421, 108, 150);
    previous.interactive = true;
    previous.buttonMode = true;
    previous.on("pointerup", () => {
      this.updateLevel(-1);
    });
    this.stage.addChild(previous);
  }

  updateLevel(delta) {
    this.level += delta;
    if (this.level > this.maxLevel) {
      this.level = 0;
    }
    if (this.level < 0) {
      this.level = this.maxLevel;
    }
    this.updatePreview();
  }
}
