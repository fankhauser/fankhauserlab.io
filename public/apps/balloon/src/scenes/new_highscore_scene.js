import { MenuScene } from "./menu_scene.js";

export class NewHighscoreScene {
  constructor(balloon, level, time) {
    this.balloon = balloon;

    // stage
    this.stage = new PIXI.Container();

    // setup
    this.time = time;
    this.setupLevel(level);
    this.setupBackground();
    this.setupText();
    this.playSound();

    setTimeout(() => {
      this.setupNamePrompt();
    }, 7000);
  }

  setupLevel(level) {
    this.level = level;
    this.levelData = PIXI.Loader.shared.resources.levels.data[this.level];
  }

  setupBackground() {
    this.bg = new PIXI.Sprite(
      PIXI.Loader.shared.resources.new_highscore.texture
    );
    this.bg.width = this.balloon.width;
    this.bg.height = this.balloon.height;
    this.stage.addChild(this.bg);
  }

  playSound() {
    PIXI.Loader.shared.resources.applause.sound.play();
  }

  setupText() {
    let style = new PIXI.TextStyle({
      fontFamily: "Arial",
      fontSize: 28,
      fill: 0xffffff,
      align: "left",
      dropShadow: true,
      dropShadowBlur: 5,
      dropShadowDistance: 3,
      dropShadowAlpha: 0.5,
      fontWeight: "500",
    });

    // level
    this.levelText = new PIXI.Text(this.levelData.name, style);
    this.levelText.style.align = "center";
    this.levelText.anchor.set(0.5, 0);
    this.levelText.x = this.balloon.width / 2;
    this.levelText.y = 210;
    this.stage.addChild(this.levelText);

    // time
    const text = `Gratulation! ${(this.time / 1000).toFixed(
      2
    )}s ist ein neuer Highscore!`;
    this.gratzText = new PIXI.Text(text, style);
    this.gratzText.style.align = "center";
    this.gratzText.anchor.set(0.5, 0);
    this.gratzText.x = this.balloon.width / 2;
    this.gratzText.y = 250;
    this.stage.addChild(this.gratzText);
  }

  setupNamePrompt() {
    const name = window.prompt("Wie ist dein Name?", "");

    // ask again if no name given
    if (!name) {
      setTimeout(() => {
        this.setupNamePrompt();
      }, 1000);
      return;
    }

    // save highscore and go back to menu
    this.balloon.setHighscoreFor(this.level, {
      name: name,
      time: this.time,
    });
    this.balloon.switchScene(new MenuScene(this.balloon, this.level));
  }
}
