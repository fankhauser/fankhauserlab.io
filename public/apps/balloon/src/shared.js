// maps a balloon type to an image
export const balloonTypeToImageMap = {
  1: "balloon_yellow",
  2: "balloon_black",
};

// scale images based on their width
export function scaleToWidth(sprite, w) {
  let scale = w / sprite.width;
  sprite.scale.x = scale;
  sprite.scale.y = scale;
}
