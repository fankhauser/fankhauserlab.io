class HHN {
  constructor(options = {}) {
    if (options.pagesFrom) {
      this.indexPages(options.pagesFrom);
    }
    if (options.tocFrom) {
      this.indexToc(options.tocFrom);
    }
    if (options.presentationFrom) {
      this.indexPresentation(options.presentationFrom);
    }
  }

  indexPages(selector) {
    this.pages = document.querySelectorAll(selector);
  }

  // render TOC based on headings
  indexToc = function (selector) {
    this.toc = [];
    for (var i = 0; i < this.pages.length; i++) {
      const page = this.pages[i];
      const heading = page.querySelector(selector);
      if (!heading) {
        continue;
      }
      const id = `page${i + 1}`;
      page.id = id;
      this.toc.push({
        type: heading.tagName.toLowerCase(),
        title: heading.innerText,
        id: id,
        href: "#" + id,
      });
    }
  };

  addPageSnippet(snippet, options = {}) {
    // loop over each page
    for (var i = 0; i < this.pages.length; i++) {
      if (options.skip && options.skip.indexOf(i) > -1) {
        continue;
      }

      // replace vars
      let html;
      html = snippet.replace("{pages}", this.pages.length);
      html = html.replace("{page}", i + 1);

      this.pages[i].insertAdjacentHTML("beforeend", html);
    }
  }

  // render TOC based on headings
  addTocSnippet = function (target, snippet, options = {}) {
    // render
    var toc = "";
    for (var i = 0; i < this.toc.length; i++) {
      const entry = this.toc[i];
      toc += snippet
        .replace("{type}", entry.type)
        .replace("{title}", entry.title)
        .replace("{id}", entry.id)
        .replace("{href}", entry.href);
    }

    // apply
    const tocs = document.querySelectorAll(target);
    for (var i = 0; i < tocs.length; i++) {
      tocs[i].innerHTML = toc;
    }
  };

  static fetchAction(event, name, data = {}) {
    event.preventDefault();
    return fetch(`actions/${name}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  }

  static toggleClass(selector, klass) {
    const elements = document.querySelectorAll(selector);
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.toggle(klass);
    }
  }

  static setupPrintView() {
    // append static class to slides
    const elements = document.querySelectorAll(".slide");
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.toggle("static");
    }

    // available height - header (60) and footer (60)
    const maxHeight = elements[0].offsetHeight - 120;

    // iterate each slide
    for (var i = 0; i < elements.length; i++) {
      const slide = elements[i];
      const children = slide.children;
      var consumedHeight = 0;

      // iterate content
      for (var c = 0; c < children.length; c++) {
        const child = children[c];
        const height = child.offsetHeight;

        if (child.className === "header") {
          continue;
        }
        if (child.className === "footer") {
          continue;
        }
        consumedHeight += height;
      }

      // check
      if (consumedHeight > maxHeight) {
        console.warn(slide, "is overfull");
      }
    }
  }
}

// config
window.MathJax = {
  tex: {
    inlineMath: [
      ["$", "$"],
      ["\\(", "\\)"],
    ],
  },
  chtml: {
    fontURL: "/assets/core",
  },
  svg: {
    fontCache: "global",
  },
};

// d3 animation config
window.D3Config = {
  fullWidth: 970,
  fullHeight: 560,
};

class HHNPresentation {
  constructor(options = {}) {
    // config
    this.slidePosition = -1;
    this.elementPosition = -1;
    this.snippets = [];

    // index
    this.index(options.from);
    this.setupKeyBindings();
  }

  start() {
    let slideIndex = parseInt(location.hash.substr(1)) || 1;
    this.nextSlide(slideIndex);
  }

  index(selector) {
    this.slides = document.querySelectorAll(selector);
    this.sequence = [];
    for (var i = 0; i < this.slides.length; i++) {
      const slide = this.slides[i];

      // slide
      const id = `slide${i + 1}`;
      slide.id = id;
      slide.classList.add("inactive");
      const entry = {
        id: id,
        ref: slide,
        elements: [],
      };

      // elements
      const nodeElements = slide.querySelectorAll('*[class^="at"]');
      let elements = [];
      for (let i = 0; i < nodeElements.length; i++) {
        elements.push(nodeElements[i]);
      }
      elements.sort((a, b) => {
        const sa = a.className.baseVal ? a.className.baseVal : a.className;
        const sb = b.className.baseVal ? b.className.baseVal : b.className;
        return sa.localeCompare(sb);
      });

      for (var e = 0; e < elements.length; e++) {
        const element = elements[e];
        const eid = `slide${i + 1}/element${e + 1}`;
        element.id = eid;
        entry.elements.push({
          id: eid,
          ref: element,
        });
      }
      this.sequence.push(entry);
    }
  }

  setupKeyBindings() {
    document.addEventListener("keydown", (e) => {
      switch (e.key) {
        case "ArrowRight":
          this.nextSlide(+1);
          break;
        case "ArrowLeft":
          this.nextSlide(-1);
          break;
        case "ArrowDown":
          this.nextElement();
          break;
        case "ArrowUp":
          this.previousElement();
          break;
        case "p":
          this.print();
          break;
      }
    });
  }

  // forward or backward
  nextSlide(delta) {
    // next position candidate
    const nextPosition = this.slidePosition + delta;

    // at end
    if (nextPosition > this.sequence.length - 1) {
      return;
    }

    // at start
    if (nextPosition < 0) {
      return;
    }

    // hide current
    if (this.currentSlide) {
      this.currentSlide.ref.classList.add("inactive");
      for (let i = 0; i < this.currentSlide.elements.length; i++) {
        this.currentSlide.elements[i].ref.classList.remove("active");
      }
    }

    // next
    this.slidePosition = nextPosition;
    this.elementPosition = -1;
    this.currentSlide.ref.classList.remove("inactive");
    this.updatePageSnippets();
    location.hash = "#" + (this.slidePosition + 1);
  }

  // forward only
  nextElement() {
    // next candidate
    const nextElementPosition = this.elementPosition + 1;

    // at end
    if (nextElementPosition > this.currentSlide.elements.length - 1) {
      return;
    }

    // next
    this.elementPosition = nextElementPosition;
    this.currentSlide.elements[this.elementPosition].ref.classList.add(
      "active"
    );
    this.updatePageSnippets();
  }

  previousElement() {
    // next candidate
    const nextElementPosition = this.elementPosition - 1;

    // at start
    if (nextElementPosition < -1) {
      return;
    }

    // hide last
    this.currentSlide.elements[this.elementPosition].ref.classList.remove(
      "active"
    );

    // next
    this.elementPosition = nextElementPosition;
    this.updatePageSnippets();
  }

  // get current slide
  get currentSlide() {
    return this.sequence[this.slidePosition];
  }

  addPageSnippet(snippet, options = {}) {
    // parse template
    var template = document.createElement("template");
    template.innerHTML = snippet.trim();
    let ref = template.content.firstChild;

    // store
    this.snippets.push({
      snippet: snippet,
      options: options,
      ref: ref,
    });
  }

  updatePageSnippets() {
    for (let s = 0; s < this.snippets.length; s++) {
      const snippet = this.snippets[s];

      if (
        snippet.options.skip &&
        snippet.options.skip.indexOf(this.slidePosition) > -1
      ) {
        continue;
      }

      // copy original template
      let html = (" " + snippet.snippet).slice(1);

      // replace vars
      html = html.replace("{pages}", this.sequence.length);
      html = html.replace("{page}", this.slidePosition + 1);
      html = html.replace("{elements}", this.currentSlide.elements.length);
      html = html.replace("{element}", this.elementPosition + 1);

      // update existing snippet
      snippet.ref.innerHTML = html;
      this.currentSlide.ref.appendChild(snippet.ref);
    }
  }

  print() {
    // change body height
    const body = document.querySelector("body");
    body.style.height = "auto";

    // show all slides
    for (var i = 0; i < this.sequence.length; i++) {
      const entry = this.sequence[i];
      entry.ref.classList.remove("inactive");

      // show all entries
      for (let e = 0; e < entry.elements.length; e++) {
        const element = entry.elements[e];

        // remove atX
        for (let a = 0; a < element.ref.classList.length; a++) {
          const name = element.ref.classList[a];
          if (name.startsWith("at")) {
            element.ref.classList.remove(name);
          }
        }

        // remove animations
        element.ref.classList.remove("fadein");
      }
    }

    window.print();
  }
}

// sequencer
class HHNSequencer extends HTMLElement {
  constructor() {
    super();
    this._data = {};
    this._step = 0;
    this._maxStep = 0;
    this._maxTime = 0;
    this._maxStates = 0;
    this._stateTables = {};
    this._dimensions = {
      padding: 40,
      timePaddingTop: 20,
      timePaddingBottom: 30,
      componentWidth: 140,
      componentHeight: 40,
      legendHeight: 15,
      stateTableWidth: 140,
      stateTableElementHeight: 20,
      stateTablePadding: 10,
    };
  }

  async connectedCallback() {
    // setup dimensions
    this.style.width = this.getAttribute("width");
    this._width = this.offsetWidth;

    // setup top bar
    this._setupTopBar();

    // load sequence data
    await this._fetchData();

    this._setupStage();
    this._setupStateTable();
    this._setupTimelines();
    this._setupComponents();
    this._advanceToStep();
  }

  _setupTopBar() {
    // bar
    this._topbar = document.createElement("div");
    this._topbar.classList.add("topbar");
    this.appendChild(this._topbar);

    // step label
    this._stepLabel = document.createElement("div");
    this._stepLabel.classList.add("label");
    this._stepLabel.textContent = "Loading ...";
    this._topbar.appendChild(this._stepLabel);

    // button container
    let buttons = document.createElement("div");
    buttons.classList.add("buttons");
    this._topbar.appendChild(buttons);

    // reset button
    this._resetButton = document.createElement("button");
    this._resetButton.textContent = "Reset";
    this._resetButton.addEventListener("click", (e) => {
      this._resetToStart();
    });
    buttons.appendChild(this._resetButton);

    // previous button
    let previousIcon = this._nextIcon();
    previousIcon.setAttribute("transform", "rotate(180)");
    this._previousStepButton = document.createElement("button");
    this._previousStepButton.appendChild(previousIcon);
    this._previousStepButton.addEventListener("click", (e) => {
      this._clearLastStep();
    });
    buttons.appendChild(this._previousStepButton);

    // next button
    let nextIcon = this._nextIcon();
    this._nextStepButton = document.createElement("button");
    this._nextStepButton.appendChild(nextIcon);
    this._nextStepButton.addEventListener("click", (e) => {
      this._setupNextStep();
    });
    buttons.appendChild(this._nextStepButton);

    // description
    this._subbar = document.createElement("div");
    this._subbar.classList.add("subbar");
    this.appendChild(this._subbar);

    // description label
    this._descriptionLabel = document.createElement("div");
    this._descriptionLabel.classList.add("label");
    this._descriptionLabel.innerHTML =
      "Click <b>next</b> button to go to <b>first step</b>";
    this._subbar.appendChild(this._descriptionLabel);
  }

  _setupStage() {
    // svg
    this._stage = this._svgCreate("svg");
    this._stage.setAttribute("width", this._width + "px");
    this._stage.setAttribute("height", this._height + "px");
    this.appendChild(this._stage);

    // defs
    const defs = this._svgCreate("defs");
    this._stage.appendChild(defs);

    // arrow
    const arrow = this._svgCreate("marker");
    arrow.setAttribute("id", "arrow");
    arrow.setAttribute("viewBox", "0 0 10 10");
    arrow.setAttribute("refX", "9");
    arrow.setAttribute("refY", "5");
    arrow.setAttribute("markerWidth", "6");
    arrow.setAttribute("markerHeight", "6");
    arrow.setAttribute("orient", "auto-start-reverse");
    defs.appendChild(arrow);

    const arrowPath = this._svgCreate("path");
    arrowPath.setAttribute("d", "M 0 0 L 10 5 L 0 10 z");
    arrow.appendChild(arrowPath);

    // cross
    const cross = this._svgCreate("marker");
    cross.setAttribute("id", "cross");
    cross.setAttribute("viewBox", "0 0 10 10");
    cross.setAttribute("refX", "10");
    cross.setAttribute("refY", "5");
    cross.setAttribute("markerWidth", "6");
    cross.setAttribute("markerHeight", "6");
    cross.setAttribute("orient", "auto-start-reverse");
    defs.appendChild(cross);

    const crossPath = this._svgCreate("path");
    crossPath.setAttribute("d", "M 1 0 L 10 9 L 9 10 L 0 1 z");
    cross.appendChild(crossPath);

    const crossPath2 = this._svgCreate("path");
    crossPath2.setAttribute("d", "M 9 0 L 10 1 L 1 10 L 0 9 z");
    cross.appendChild(crossPath2);
  }

  _setupStateTable() {
    this._redrawStateTables(this._data.state);
  }

  _setupTimelines() {
    if (!this._data.timelines) {
      return;
    }
    // group
    const g = this._svgCreate("g");
    g.classList.add("timeline");
    g.setAttribute(
      "transform",
      `translate(0 ${
        this._dimensions.padding +
        2 * this._dimensions.stateTableElementHeight * this._maxStates +
        this._dimensions.stateTablePadding +
        this._dimensions.componentHeight +
        this._dimensions.timePaddingTop
      })`
    );
    this._stage.appendChild(g);

    for (const [id, time] of Object.entries(this._data.timelines)) {
      let left = this._xFromPercent(0) - 20;
      let right = this._xFromPercent(100) + 20;

      // grid
      const timeline = this._svgCreate("line");
      timeline.classList.add("grid");
      timeline.setAttribute("x1", left);
      timeline.setAttribute("x2", right);
      timeline.setAttribute("y1", time);
      timeline.setAttribute("y2", time);
      g.appendChild(timeline);

      // nameLeft
      const nameLeft = this._svgCreate("foreignObject");
      nameLeft.classList.add("name");
      nameLeft.setAttribute("x", this._dimensions.padding);
      nameLeft.setAttribute("y", time - 8);
      nameLeft.setAttribute("width", this._dimensions.componentWidth / 2);
      nameLeft.setAttribute("height", 20);
      nameLeft.textContent = `${id} = ${time}`;
      g.appendChild(nameLeft);

      // nameRight
      const nameRight = this._svgCreate("foreignObject");
      nameRight.classList.add("name-right");
      nameRight.setAttribute("x", right - 20);
      nameRight.setAttribute("y", time - 8);
      nameRight.setAttribute("width", this._dimensions.componentWidth / 2);
      nameRight.setAttribute("height", 20);
      nameRight.textContent = `${id} = ${time}`;
      g.appendChild(nameRight);
    }
  }

  _setupComponents() {
    for (const [id, component] of Object.entries(this._data.components)) {
      // group
      const g = this._svgCreate("g");
      g.classList.add("component");
      g.setAttribute("id", `component_${id}`);
      g.setAttribute(
        "transform",
        `translate(${this._xFromPercent(component.x)} ${
          this._dimensions.padding +
          2 * this._dimensions.stateTableElementHeight * this._maxStates +
          this._dimensions.stateTablePadding
        })`
      );
      this._stage.appendChild(g);

      // box
      const rect = this._svgCreate("rect");
      rect.classList.add("box");
      rect.setAttribute("width", this._dimensions.componentWidth);
      rect.setAttribute("height", this._dimensions.componentHeight);
      rect.setAttribute("x", -this._dimensions.componentWidth / 2);
      g.appendChild(rect);

      // name
      const name = this._svgCreate("foreignObject");
      name.classList.add("name");
      name.setAttribute("x", -this._dimensions.componentWidth / 2);
      name.setAttribute("y", 9);
      name.setAttribute("width", this._dimensions.componentWidth);
      name.setAttribute("height", this._dimensions.componentHeight);
      name.textContent = component.name;
      g.appendChild(name);

      // timeline
      const timeline = this._svgCreate("line");
      timeline.classList.add("timeline");
      timeline.setAttribute("x1", 0);
      timeline.setAttribute("x2", 0);
      timeline.setAttribute("y1", this._dimensions.componentHeight);
      timeline.setAttribute(
        "y2",
        this._dimensions.componentHeight +
          this._dimensions.timePaddingTop +
          this._dimensions.timePaddingBottom +
          this._maxTime
      );
      timeline.setAttribute("marker-end", "url(#arrow)");
      g.appendChild(timeline);

      // legend
      const legend = this._svgCreate("text");
      legend.classList.add("legend");
      legend.setAttribute("x", 0);
      legend.setAttribute(
        "y",
        this._dimensions.componentHeight +
          this._dimensions.timePaddingTop +
          this._dimensions.timePaddingBottom +
          this._maxTime +
          this._dimensions.legendHeight
      );
      legend.textContent = "t";
      g.appendChild(legend);
    }
  }

  _setupNextStep() {
    // do nothing if max
    if (this._maxStep <= this._step) {
      return false;
    }
    // advance to next step
    this._step++;

    // we draw nothing for step 0
    if (this._step == 0) {
      return;
    }

    const step = this._data.steps[this._step - 1];

    // name
    this._stepLabel.textContent = `Step ${this._step}: ${step.name}`;
    this._descriptionLabel.innerHTML = step.description;

    // element storage
    step.elements = [];
    const from = this._data.components[step.from];
    const to = this._data.components[step.to];

    // step group
    const g = this._svgCreate("g");
    g.classList.add("step");
    g.setAttribute("id", `step_${this._step}`);
    g.setAttribute(
      "transform",
      `translate(0 ${
        this._dimensions.padding +
        2 * this._dimensions.stateTableElementHeight * this._maxStates +
        this._dimensions.stateTablePadding +
        this._dimensions.componentHeight +
        this._dimensions.timePaddingTop
      })`
    );
    this._stage.appendChild(g);
    step.elements.push(g);

    let fromX = this._xFromPercent(from.x);
    let toX = this._xFromPercent(to.x);
    let width = toX - fromX;
    let height = step.receiveAt - step.sendAt;
    let marker = "#arrow";

    // if message is list - stop line before received
    if (step.lost) {
      let delta = fromX - toX;
      let distance = parseInt(delta * -0.8);
      toX = fromX + distance;
      marker = "#cross";
    }

    // message
    const messageLine = this._svgCreate("line");
    messageLine.classList.add("line");
    messageLine.setAttribute("x1", fromX);
    messageLine.setAttribute("x2", toX);
    messageLine.setAttribute("y1", step.sendAt);
    messageLine.setAttribute("y2", step.receiveAt);
    messageLine.setAttribute("marker-end", `url(${marker})`);
    g.appendChild(messageLine);

    // message name group
    let positionFactor = step.messagePosition || 0.4;
    let x = parseInt(fromX + positionFactor * width);
    let y = parseInt(step.sendAt + positionFactor * height);
    const mg = this._svgCreate("g");
    mg.setAttribute("transform", `translate(${x} ${y})`);
    g.appendChild(mg);

    // title
    const title = this._svgCreate("foreignObject");
    title.classList.add("title-box");
    title.setAttribute("x", "0");
    title.setAttribute("y", -8);
    title.setAttribute("width", 200);
    title.setAttribute("height", 1);
    mg.appendChild(title);

    const span = document.createElement("span");
    span.classList.add("title");
    span.textContent = step.message;
    title.appendChild(span);

    // center message group for self referencing message
    if (from == to) {
      let offset = parseInt((span.offsetWidth - 12) / 2);
      x = fromX - offset;
      mg.setAttribute("transform", `translate(${x} ${y})`);
    }

    // state table
    this._redrawStateTables(step.state);

    this._afterRender();
  }

  _clearLastStep() {
    // do nothing if min
    if (this._step <= 0) {
      return false;
    }

    // clear current step
    var step = this._data.steps[this._step - 1];
    for (let i = 0; i < step.elements.length; i++) {
      const element = step.elements[i];
      element.remove();
    }
    // reset
    step.elements = [];

    // go back to last step
    this._step--;

    // we draw nothing for step 0
    if (this._step == 0) {
      this._stepLabel.textContent = `Ready`;
      this._descriptionLabel.innerHTML =
        "Click <b>next</b> button to go to <b>first step</b>";
      this._redrawStateTables(this._data.state);
      this._afterRender();
      return;
    }

    step = this._data.steps[this._step - 1];

    // name & description
    this._stepLabel.textContent = `Step ${this._step}: ${step.name}`;
    this._descriptionLabel.innerHTML = step.description;

    // state table
    this._redrawStateTables(step.state);
    this._afterRender();
  }

  _advanceToStep() {
    var desiredStep = this.getAttribute("step");
    if (desiredStep === "last") {
      desiredStep = this._maxStep;
    } else {
      desiredStep = parseInt(desiredStep) - 1;
    }

    // upper bound
    if (desiredStep > this._maxStep) {
      desiredStep = this._maxStep;
    }

    // lower bound
    if (desiredStep < 0) {
      desiredStep = 0;
    }

    // advance
    while (this._step < desiredStep) {
      this._setupNextStep();
    }
  }

  _resetToStart() {
    // advance
    while (this._step > 0) {
      this._clearLastStep();
    }
  }

  _redrawStateTables(state) {
    // reset
    for (const [id, g] of Object.entries(this._stateTables)) {
      g.remove();
    }

    if (!state) {
      return;
    }

    // draw
    for (const [id, kvs] of Object.entries(state)) {
      let component = this._data.components[id];

      // group
      const g = this._svgCreate("g");
      g.classList.add("statetable");
      g.setAttribute("id", `statetable_${id}`);
      g.setAttribute(
        "transform",
        `translate(${this._xFromPercent(component.x)} ${
          this._dimensions.padding +
          2 * this._dimensions.stateTableElementHeight * this._maxStates
        })`
      );
      this._stage.appendChild(g);
      this._stateTables[id] = g;

      let keys = Object.keys(kvs).sort().reverse();
      let states = keys.length;
      let height = 2 * this._dimensions.stateTableElementHeight * states;

      // box
      const rect = this._svgCreate("rect");
      rect.classList.add("box");
      rect.setAttribute("width", this._dimensions.stateTableWidth);
      rect.setAttribute("height", height);
      rect.setAttribute("x", -this._dimensions.stateTableWidth / 2);
      rect.setAttribute("y", -height);
      g.appendChild(rect);

      let y = 0;
      for (const k of keys) {
        // value
        const value = this._svgCreate("foreignObject");
        value.classList.add("value");
        value.setAttribute("x", -this._dimensions.stateTableWidth / 2);
        value.setAttribute("y", y - this._dimensions.stateTableElementHeight);
        value.setAttribute("width", this._dimensions.stateTableWidth);
        value.setAttribute("height", this._dimensions.stateTableElementHeight);
        value.textContent = kvs[k];
        g.appendChild(value);

        y -= this._dimensions.stateTableElementHeight;

        // key bg
        const rect = this._svgCreate("rect");
        rect.classList.add("bg");
        rect.setAttribute("width", this._dimensions.stateTableWidth);
        rect.setAttribute("height", this._dimensions.stateTableElementHeight);
        rect.setAttribute("x", -this._dimensions.stateTableWidth / 2);
        rect.setAttribute("y", y - this._dimensions.stateTableElementHeight);
        g.appendChild(rect);

        // key
        const key = this._svgCreate("foreignObject");
        key.classList.add("key");
        key.setAttribute("x", -this._dimensions.stateTableWidth / 2);
        key.setAttribute("y", y - this._dimensions.stateTableElementHeight);
        key.setAttribute("width", this._dimensions.stateTableWidth);
        key.setAttribute("height", this._dimensions.stateTableElementHeight);
        key.textContent = k;
        g.appendChild(key);

        y -= this._dimensions.stateTableElementHeight;

        // frame
        const frame = this._svgCreate("rect");
        frame.classList.add("frame");
        frame.setAttribute("width", this._dimensions.stateTableWidth);
        frame.setAttribute(
          "height",
          2 * this._dimensions.stateTableElementHeight
        );
        frame.setAttribute("x", -this._dimensions.stateTableWidth / 2);
        frame.setAttribute("y", y);
        g.appendChild(frame);
      }
    }
  }

  _afterRender() {
    MathJax.typeset([this]);
  }

  _xFromPercent(xp) {
    const paddingSide =
      this._dimensions.componentWidth / 2 + this._dimensions.padding;
    const width = this._width - 2 * paddingSide;
    const step = width / 100;
    const offset = xp * step;
    return parseInt(paddingSide + offset);
  }

  _svgCreate(tag) {
    return document.createElementNS("http://www.w3.org/2000/svg", tag);
  }

  // load json from src attribute
  async _fetchData() {
    // fetch
    let response = await fetch(this.getAttribute("src"));
    this._data = await response.json();

    // max step
    this._maxStep = this._data.steps.length;

    // maximums
    for (let i = 0; i < this._data.steps.length; i++) {
      const step = this._data.steps[i];

      // max time
      if (step.receiveAt > this._maxTime) {
        this._maxTime = step.receiveAt;
      }

      // max states
      for (const [id, kvs] of Object.entries(step.state)) {
        let kvs = step.state[id];
        if (!kvs) {
          continue;
        }

        let states = Object.keys(kvs).length;
        if (states > this._maxStates) {
          this._maxStates = states;
        }
      }
    }

    // calculate total height
    this._height =
      2 * this._dimensions.padding +
      2 * this._dimensions.stateTableElementHeight * this._maxStates +
      this._dimensions.stateTablePadding +
      this._dimensions.timePaddingTop +
      this._dimensions.timePaddingBottom +
      this._dimensions.componentHeight +
      this._maxTime +
      this._dimensions.legendHeight;

    // update label
    this._stepLabel.textContent = `Ready`;
  }

  _nextIcon() {
    // create icon
    let icon = this._svgCreate("svg");
    icon.classList.add("icon");
    icon.setAttribute("width", 14);
    icon.setAttribute("height", 14);
    icon.setAttribute("viewBox", "0 0 10 10");

    // add arrow
    const arrow = this._svgCreate("path");
    arrow.setAttribute("d", "M 0 0 L 10 5 L 0 10 z");
    icon.appendChild(arrow);

    // add dash
    const dash = this._svgCreate("path");
    dash.setAttribute("d", "M 8 0 L 10 0 L 10 10 L 8 10 z");
    icon.appendChild(dash);

    return icon;
  }
}
window.customElements.define("hhn-sequencer", HHNSequencer);

// spreadsheet
class HHNSpreadsheet extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    // header
    this._header = document.createElement("div");
    this._header.id = "header";

    // table
    this._table = document.createElement("table", { is: "hhn-table" });
    this._table._src = this.getAttribute("src");

    // show
    this.appendChild(this._header);
    this.appendChild(this._table);
  }
}

// TODO: - Formulas
class HHNTable extends HTMLTableElement {
  constructor() {
    super();

    // table source
    this._src = null;
  }

  connectedCallback() {
    // reload data
    this._reload(this._src);

    // listen for new events
    this.addEventListener("hhn-table:NewEvent", (event) => {
      this._processNewEvent(event.detail);
    });
  }

  async _reload(src) {
    // remove all
    for (let i = 0; i < this.children.length; i++) {
      this.children[i].remove();
    }

    // fetch data
    const response = await fetch(src, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    try {
      this._data = await response.json();
    } catch {
      this._data = this._newEmptyData();
    }

    // create table from data
    this._addHeader(this._data.header);
    this._addRows(this._data.rows);
  }

  _newEmptyData() {
    return {
      version: 0,
      header: ["Header A", "Header B"],
      rows: [
        [
          { type: "text", value: "Foo" },
          { type: "text", value: "Bar" },
        ],
      ],
      events: [],
    };
  }

  _addHeader(header) {
    const tr = document.createElement("tr", {
      is: "hhn-table-tr",
    });
    for (let i = 0; i < header.length; i++) {
      const value = header[i];
      const th = document.createElement("th", { is: "hhn-table-th" });
      th.textContent = value;
      tr.appendChild(th);
    }
    this.appendChild(tr);
  }

  _addRows(rows) {
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];
      this._addRow(row);
    }
  }

  _addRow(row) {
    let tr = document.createElement("tr", {
      is: "hhn-table-tr",
    });
    for (let i = 0; i < row.length; i++) {
      const cell = row[i];
      const td = document.createElement("td", { is: "hhn-table-td" });
      td.textContent = cell.value;
      tr.appendChild(td);
    }
    this.appendChild(tr);
  }

  async _processNewEvent(event) {
    // next version candidate (could be rejected by server)
    const currentVersion = this._data.version;
    const nextVersion = this._data.version + 1;
    event.version = nextVersion;

    // add timestamp
    event.timestamp = Date.now();

    console.log(event);

    // apply event to state
    this["_applyEvent" + event.name](event);

    // append to events
    this._data.events.push(event);

    // set version (could be rejected by server)
    this._data.version = nextVersion;

    // send event to backend
    const response = await fetch("/api/store/version", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        src: this._src,
        version: currentVersion,
        data: this._data,
      }),
    });
    const status = await response.json();

    // check if server updated to correct version
    if (status.version !== nextVersion) {
      throw "invalid status returned, reload table";
    }

    console.log(this._data);
  }

  // events
  _applyEventUpdateHeaderCell(event) {
    this._data.header[event.cell] = event.data;
  }

  _applyEventUpdateDataCell(event) {
    // get current cell
    const cell = this._data.rows[event.row - 1][event.cell];

    // change data type
    if (event.type) {
      cell.type = event.type;
    }

    // handle different cell types
    let value = "";
    switch (cell.type) {
      case "text":
        cell.input = String(event.data);
        cell.value = String(event.data);
        value = cell.value;
        break;
      case "integer":
        cell.value = parseInt(event.data);
        value = String(cell.value);
        break;
      case "float":
        cell.value = parseFloat(event.data);
        value = cell.value.toFixed(2);
        break;
      case "formula":
        cell.input = event.data;
        const result = this._evaluateFormula(cell.input);
        cell.value = result;
        value = result;
        break;
    }

    // update ui
    const td = this.rows[event.row].children[event.cell];
    td.innerHTML = value;
  }

  // calculate a formula
  _evaluateFormula(formula) {
    let table = this;
    return (function () {
      // setup context
      var cell = function (rowIndex, cellIndex) {
        const c = table._data.rows[rowIndex][cellIndex];
        return c.value;
      };

      // evaluate
      return eval(formula);
    })();
  }

  _applyEventAddRow(event) {
    // append position
    const position = event.position === "before" ? "before" : "after";

    // create ui
    const tr = document.createElement("tr", { is: "hhn-table-tr" });
    for (let i = 0; i < this._data.header.length; i++) {
      const header = this._data.header[i];
      const td = document.createElement("td", { is: "hhn-table-td" });
      tr.appendChild(td);
    }
    this.rows[event.row][position](tr);
    tr.children[0].focus();

    // add data
    const offset = event.position === "before" ? 1 : 0;
    const row = [];
    for (let i = 0; i < this._data.header.length; i++) {
      const header = this._data.header[i];
      row.push({ type: "text", value: "" });
    }
    this._data.rows.splice(event.row - offset, 0, row);
  }

  _applyEventDeleteRow(event) {
    // update ui
    const tr = this.rows[event.row];
    tr.remove();

    // remove data
    this._data.rows.splice(event.row - 1, 1);
  }

  _applyEventAddColumn(event) {
    // append position
    const position = event.position === "left" ? "before" : "after";

    // create ui
    const th = document.createElement("th", { is: "hhn-table-th" });
    this.rows[0].children[event.column][position](th);
    th.focus();

    for (let i = 1; i < this.rows.length; i++) {
      const tr = this.rows[i];
      const td = document.createElement("td", { is: "hhn-table-td" });
      tr.children[event.column][position](td);
    }

    // add data
    const offset = event.position === "left" ? 0 : 1;
    this._data.header.splice(event.column + offset, 0, "");

    const row = [];
    for (let i = 0; i < this._data.rows.length; i++) {
      const row = this._data.rows[i];
      row.splice(event.column + offset, 0, { type: "text", value: "" });
    }
  }

  _applyEventDeleteColumn(event) {
    // update ui
    for (let i = 0; i < this.rows.length; i++) {
      const tr = this.rows[i];
      tr.children[event.column].remove();
    }

    // remove data
    for (let i = 0; i < this._data.rows.length; i++) {
      const row = this._data.rows[i];
      row.splice(event.column, 1);
    }
    this._data.header.splice(event.column, 1);
  }
}

class HHNTableRow extends HTMLTableRowElement {
  constructor() {
    super();
  }

  connectedCallback() {}
}

class HHNTableHeaderCell extends HTMLTableCellElement {
  constructor() {
    super();

    this.setAttribute("contenteditable", "true");
  }

  connectedCallback() {
    let cell = this;
    let textContentIn = "";

    this.addEventListener("focusin", (e) => {
      textContentIn = cell.textContent;
    });

    this.addEventListener("focusout", (e) => {
      let textContentOut = cell.textContent;
      if (textContentOut == textContentIn) {
        return;
      }

      // update cell
      this._dispatchNewEvent({
        name: "UpdateHeaderCell",
        data: textContentOut,
        cell: this.cellIndex,
      });
    });

    this.addEventListener("contextmenu", (e) => {
      e.preventDefault();
      const dropdown = document.createElement("hhn-dropdown");
      dropdown.setAttribute("x", e.pageX);
      dropdown.setAttribute("y", e.pageY);

      dropdown.addEntry("Insert row below", (e) => {
        this._dispatchNewEvent({
          name: "AddRow",
          row: this.parentElement.rowIndex,
          position: "after",
        });
      });

      dropdown.addEntry("Insert column right", (e) => {
        this._dispatchNewEvent({
          name: "AddColumn",
          column: this.cellIndex,
          position: "right",
        });
      });

      dropdown.addEntry("Insert column left", (e) => {
        this._dispatchNewEvent({
          name: "AddColumn",
          column: this.cellIndex,
          position: "left",
        });
      });

      dropdown.addEntry("Delete column", (e) => {
        this._dispatchNewEvent({
          name: "DeleteColumn",
          column: this.cellIndex,
        });
      });

      document.body.appendChild(dropdown);
    });
  }

  _dispatchNewEvent(event) {
    this.dispatchEvent(
      new CustomEvent("hhn-table:NewEvent", {
        bubbles: true,
        detail: event,
      })
    );
  }
}

class HHNTableDataCell extends HTMLTableCellElement {
  constructor() {
    super();

    this.setAttribute("contenteditable", "true");
  }

  connectedCallback() {
    let table = this.parentElement.parentElement;
    let cell =
      table._data.rows[this.parentElement.rowIndex - 1][this.cellIndex];
    let td = this;
    let textContentIn = "";

    this.addEventListener("focusin", (e) => {
      if (cell.input) {
        td.textContent = cell.input;
      }

      textContentIn = td.textContent;
    });

    this.addEventListener("focusout", (e) => {
      let textContentOut = td.textContent;
      if (textContentOut == textContentIn && !cell.input) {
        return;
      }

      // update cell
      this._dispatchNewEvent({
        name: "UpdateDataCell",
        data: textContentOut,
        row: this.parentElement.rowIndex,
        cell: this.cellIndex,
      });
    });

    this.addEventListener("contextmenu", (e) => {
      e.preventDefault();
      const dropdown = document.createElement("hhn-dropdown");
      dropdown.setAttribute("x", e.pageX);
      dropdown.setAttribute("y", e.pageY);

      dropdown.addEntry("Insert row above", (e) => {
        this._dispatchNewEvent({
          name: "AddRow",
          row: this.parentElement.rowIndex,
          position: "before",
        });
      });

      dropdown.addEntry("Insert row below", (e) => {
        this._dispatchNewEvent({
          name: "AddRow",
          row: this.parentElement.rowIndex,
          position: "after",
        });
      });

      dropdown.addEntry("Delete row", (e) => {
        this._dispatchNewEvent({
          name: "DeleteRow",
          row: this.parentElement.rowIndex,
        });
      });

      dropdown.addEntry("Set type: text", (e) => {
        this._dispatchNewEvent({
          name: "UpdateDataCell",
          row: this.parentElement.rowIndex,
          cell: this.cellIndex,
          type: "text",
          data: td.innerText,
        });
      });

      dropdown.addEntry("Set type: integer", (e) => {
        this._dispatchNewEvent({
          name: "UpdateDataCell",
          row: this.parentElement.rowIndex,
          cell: this.cellIndex,
          type: "integer",
          data: td.innerText,
        });
      });

      dropdown.addEntry("Set type: float", (e) => {
        this._dispatchNewEvent({
          name: "UpdateDataCell",
          row: this.parentElement.rowIndex,
          cell: this.cellIndex,
          type: "float",
          data: td.innerText,
        });
      });

      dropdown.addEntry("Set type: formula", (e) => {
        this._dispatchNewEvent({
          name: "UpdateDataCell",
          row: this.parentElement.rowIndex,
          cell: this.cellIndex,
          type: "formula",
          data: td.innerText,
        });
      });

      document.body.appendChild(dropdown);
    });
  }

  _dispatchNewEvent(event) {
    this.dispatchEvent(
      new CustomEvent("hhn-table:NewEvent", {
        bubbles: true,
        detail: event,
      })
    );
  }
}

// spreadsheet
class HHNDropdown extends HTMLElement {
  constructor() {
    super();

    this._entries = [];
  }

  addEntry(name, callback) {
    const entry = document.createElement("div");
    entry.textContent = name;
    entry.addEventListener("click", callback);
    this._entries.push(entry);
  }

  connectedCallback() {
    // position
    this.style.top = this.getAttribute("y") + "px";
    this.style.left = this.getAttribute("x") + "px";

    // show
    for (let i = 0; i < this._entries.length; i++) {
      const entry = this._entries[i];
      this.appendChild(entry);
    }

    // close
    const dropdown = this;
    document.body.addEventListener(
      "click",
      (e) => {
        dropdown.remove();
      },
      false
    );
  }
}

class HHNKart extends HTMLElement {
  constructor() {
    super();

    this._width = 800;
    this._height = 400;
  }

  connectedCallback() {
    // setup background
    this._background = document.createElement("div");
    this._background.classList.add("background");
    this._background.style.width = this._width + "px";
    this._background.style.height = this._height + "px";
    this.appendChild(this._background);

    // add yourself as player
    this._player = document.createElement("hhn-kart-player");
    this.appendChild(this._player);

    // add user controls
    let self = this;
    this._keys = {};
    document.addEventListener("keydown", (event) => {
      event.preventDefault();
      self._keys[event.key] = true;
    });
    document.addEventListener("keyup", (event) => {
      event.preventDefault();
      self._keys[event.key] = false;
    });

    // start loop
    window.requestAnimationFrame(() => this._loop());
  }

  _loop() {
    if (this._keys["ArrowUp"]) {
      this._player._accelerate();
    }
    if (this._keys["ArrowLeft"]) {
      this._player._left();
    }
    if (this._keys["ArrowRight"]) {
      this._player._right();
    }

    this._player._updatePosition();
    window.requestAnimationFrame(() => this._loop());
  }
}

class HHNKartPlayer extends HTMLElement {
  constructor() {
    super();

    this._maxSpeed = 5;
    this._decreaseSpeed = 0.1;
    this._steeringSensitivity = 3;

    this._width = 10;
    this._height = 20;

    this._x = 100;
    this._y = 100;
    this._speed = 5;
    this._direction = 45;
  }

  connectedCallback() {
    // setup kart
    this._kart = document.createElement("div");
    this._kart.classList.add("kart");
    this._kart.style.width = this._width + "px";
    this._kart.style.height = this._height + "px";
    this.appendChild(this._kart);
  }

  _accelerate() {
    this._speed += 2;
  }

  _left() {
    this._direction -= this._steeringSensitivity;

    if (this._direction < 0) {
      this._direction = 359;
    }
  }

  _right() {
    this._direction += this._steeringSensitivity;

    if (this._direction > 360) {
      this._direction = 0;
    }
  }

  _updatePosition() {
    // limit max speed
    if (this._speed > this._maxSpeed) {
      this._speed = this._maxSpeed;
    }

    // decrease speed
    this._speed = this._speed - this._decreaseSpeed;
    if (this._speed < 0) {
      this._speed = 0;
    }

    // move with speed in direction
    let directionInRad = this._degreeToRad(this._direction);
    this._x = this._x + Math.sin(directionInRad) * this._speed;
    this._y = this._y + Math.cos(directionInRad) * this._speed;

    // apply
    this.style.left = this._x + "px";
    this.style.bottom = this._y + "px";
    this._kart.style.transform = `translate(${-this._width / 2}px,${
      -this._height / 2
    }px) rotate(${this._direction}deg)`;
  }

  _degreeToRad(degree) {
    return (degree * Math.PI) / 180;
  }
}

window.customElements.define("hhn-spreadsheet", HHNSpreadsheet);
window.customElements.define("hhn-table", HHNTable, { extends: "table" });
window.customElements.define("hhn-table-tr", HHNTableRow, { extends: "tr" });
window.customElements.define("hhn-table-th", HHNTableHeaderCell, {
  extends: "th",
});
window.customElements.define("hhn-table-td", HHNTableDataCell, {
  extends: "td",
});
window.customElements.define("hhn-dropdown", HHNDropdown);

window.customElements.define("hhn-kart", HHNKart);
window.customElements.define("hhn-kart-player", HHNKartPlayer);
